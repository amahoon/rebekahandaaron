<?php
  $base_url = "$_SERVER[DOCUMENT_ROOT]$_SERVER[REQUEST_URI]";
  $views_url = 'includes/views/';

  include 'includes/config.php';

  error_reporting( E_ALL );
?>
<!doctype html>
<html lang="en" dir="ltr" class="loading">
<head>
<?php include 'includes/metatags.php'; ?>
<title>Rebekah and Aaron | We're Getting Married</title>

<script type="text/javascript" src="node_modules/jquery/dist/jquery.min.js"></script>
<script type="text/javascript" src="node_modules/jquery-match-height/jquery.matchHeight.js"></script>

<!-- loader -->
<script type="text/javascript" src="node_modules/particles.js/particles.js"></script>
<script type="text/javascript" src="dist/js/particles.min.js"></script>

<!-- include css files -->
<?php include 'includes/css.php'; ?>

</head>
<body class="loading">
  <!-- DEVELOPMENT -->
  <?php // include $views_url . 'dev/window-measurements.php'; ?>
  <?php // include $views_url . 'dev/breakpoints.php'; ?>

  <?php include $views_url . 'loader.php'; ?>

  <!-- HEADER -->
  <?php include $views_url . 'header.php'; ?>

  <!-- SECTIONS -->
  <?php include $views_url . 'sections/ourstory.php'; ?>
  <div class="parallax parallax--1"></div>
  <?php include $views_url . 'sections/event.php'; ?>
  <?php include $views_url . 'sections/googlemap.php'; ?>
  <?php include $views_url . 'sections/bridalparty.php'; ?>
  <div class="parallax parallax--2"></div>
  <?php include $views_url . 'sections/rsvp.php'; ?>
  <?php include $views_url . 'sections/registry.php'; ?>
  <?php include $views_url . 'sections/gallery.php'; ?>

  <!-- FOOTER -->
  <?php include $views_url . 'footer.php'; ?>

  <!-- include javascript files -->
  <script type="text/javascript" src="dist/js/svg.min.js"></script>
  <?php include 'includes/scripts.php'; ?>
</body>
</html>