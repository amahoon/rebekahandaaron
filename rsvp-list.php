<?php
  $base_url = "$_SERVER[DOCUMENT_ROOT]$_SERVER[REQUEST_URI]";
  $views_url = 'includes/views/';

  include 'includes/config.php';

  // echo '<div style="background-color: white;"><pre>' . var_export(get_included_files(),TRUE) . '</pre></div>';
  error_reporting( E_ALL );
?>
<!doctype html>
<html lang="en" dir="ltr">
<head>
<?php include 'includes/metatags.php'; ?>
<title>The RSVPs | Rebekah and Aaron are getting married</title>

<script type="text/javascript" src="node_modules/jquery/dist/jquery.min.js"></script>
<script type="text/javascript" src="node_modules/jquery-match-height/jquery.matchHeight.js"></script>

<!-- include css files -->
<?php include 'includes/css.php'; ?>

</head>
<body class="rsvp-list">
  <!-- HEADER -->
  <?php include $views_url . 'rsvp-header.php'; ?>

  <?php include $views_url . 'rsvp-count.php'; ?>
  <?php include $views_url . 'whohasRSVPed.php'; ?>

  <!-- FOOTER -->
  <?php include $views_url . 'footer.php'; ?>

  <!-- include javascript files -->
  <script type="text/javascript" src="dist/js/svg.min.js"></script>
  <script type="text/javascript" src="dist/js/rsvp-list.min.js"></script>
</body>
</html>