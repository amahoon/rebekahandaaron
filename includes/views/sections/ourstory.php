<?php $images = 'images/temp/pexels/'; ?>
<section class="ourstory" id="ourstory">
  <h2 class="hidden">Our Story</h2>
  <div class="slick-carousel ourstory">
    <div class="slide">
      <div class="title-header">
        <h3 class="story-year">2013<span class="hidden"> | { year title }</span></h3>
        <!-- <div class="divider light"></div> -->
        <?php include 'includes/views/hearts.php'; ?>
        <p class="title">{ title summarizing year }</p>
      </div>
      <div class="readmore">
        <div class="marker">
          <p>read more</p>
          <img class="svg" src="images/marker.svg" alt="Read More link">
        </div>
      </div>
      <div class="year-details closed">
        <p class="year">2013</p>
        <p class="title">{ title summarizing year }</p>
        <!-- <div class="divider dark"></div> -->
        <?php include 'includes/views/hearts.php'; ?>
        <p class="writeup">Phasellus aliquet augue mauris, eu sollicitudin arcu fringilla sed. Quisque eu massa semper, gravida erat eget, dignissim est. Curabitur sed velit mauris. Nullam vitae fringilla nisl. Sed finibus neque et erat iaculis, at euismod urna pretium. Praesent ultricies nulla leo, eget hendrerit velit luctus eget.<br><br>Etiam diam nisi, convallis a dapibus quis, gravida ac dui. Ut laoreet dolor odio, et mattis nibh auctor eu. Aliquam nec facilisis massa, a mollis eros. Quisque vestibulum mauris at lacus placerat, eget egestas quam ullamcorper. Sed ac ligula mauris. Suspendisse convallis, urna ac ullamcorper consectetur, quam arcu tempus orci, ut condimentum justo enim eu nibh. ficitur laoreet.</p>
        <div class="close">X</div>
      </div>
      <!-- baby -->
      <!-- <img src="images/temp/pexels/pexels-photo-789786.jpeg" class="background"> -->
      <div style="background-image: url(<?=$images?>pexels-photo-789786.jpeg)" class="background"></div>
    </div>
    <div class="slide">
      <div class="title-header">
        <h3 class="story-year">2014<span class="hidden"> | { year title }</span></h3>
        <!-- <div class="divider light"></div> -->
        <?php include 'includes/views/hearts.php'; ?>
        <p class="title">{ title summarizing year }</p>
      </div>
      <div class="readmore">
        <div class="marker">
          <p>read more</p>
          <img class="svg" src="images/marker.svg" alt="Read More link">
        </div>
      </div>
      <div class="year-details closed">
        <p class="year">2014</p>
        <p class="title">{ title summarizing year }</p>
        <!-- <div class="divider dark"></div> -->
        <?php include 'includes/views/hearts.php'; ?>
        <p class="writeup">Phasellus aliquet augue mauris, eu sollicitudin arcu fringilla sed. Quisque eu massa semper, gravida erat eget, dignissim est. Curabitur sed velit mauris. Nullam vitae fringilla nisl. Sed finibus neque et erat iaculis, at euismod urna pretium. Praesent ultricies nulla leo, eget hendrerit velit luctus eget.<br><br>Etiam diam nisi, convallis a dapibus quis, gravida ac dui. Ut laoreet dolor odio, et mattis nibh auctor eu. Aliquam nec facilisis massa, a mollis eros. Quisque vestibulum mauris at lacus placerat, eget egestas quam ullamcorper. Sed ac ligula mauris. Suspendisse convallis, urna ac ullamcorper consectetur, quam arcu tempus orci, ut condimentum justo enim eu nibh. ficitur laoreet.</p>
        <div class="close">X</div>
      </div>
      <!-- puppy -->
      <!-- <img src="images/temp/pexels/pexels-photo-460823.jpeg" class="background"> -->
      <div style="background-image: url(<?=$images?>pexels-photo-460823.jpeg)" class="background"></div>
    </div>
    <div class="slide">
      <div class="title-header">
        <h3 class="story-year">2015<span class="hidden"> | { year title }</span></h3>
        <!-- <div class="divider light"></div> -->
        <?php include 'includes/views/hearts.php'; ?>
        <p class="title">{ title summarizing year }</p>
      </div>
      <div class="readmore">
        <div class="marker">
          <p>read more</p>
          <img class="svg" src="images/marker.svg" alt="Read More link">
        </div>
      </div>
      <div class="year-details closed">
        <p class="year">2015</p>
        <p class="title">{ title summarizing year }</p>
        <!-- <div class="divider dark"></div> -->
        <?php include 'includes/views/hearts.php'; ?>
        <p class="writeup">Phasellus aliquet augue mauris, eu sollicitudin arcu fringilla sed. Quisque eu massa semper, gravida erat eget, dignissim est. Curabitur sed velit mauris. Nullam vitae fringilla nisl. Sed finibus neque et erat iaculis, at euismod urna pretium. Praesent ultricies nulla leo, eget hendrerit velit luctus eget.<br><br>Etiam diam nisi, convallis a dapibus quis, gravida ac dui. Ut laoreet dolor odio, et mattis nibh auctor eu. Aliquam nec facilisis massa, a mollis eros. Quisque vestibulum mauris at lacus placerat, eget egestas quam ullamcorper. Sed ac ligula mauris. Suspendisse convallis, urna ac ullamcorper consectetur, quam arcu tempus orci, ut condimentum justo enim eu nibh. ficitur laoreet.</p>
        <div class="close">X</div>
      </div>
      <!-- VW -->
      <!-- <img src="images/temp/pexels/pexels-photo-704557.jpeg" class="background"> -->
      <div style="background-image: url(<?=$images?>pexels-photo-704557.jpeg)" class="background"></div>
    </div>
    <div class="slide">
      <div class="title-header">
        <h3 class="story-year">2016<span class="hidden"> | { year title }</span></h3>
        <!-- <div class="divider light"></div> -->
        <?php include 'includes/views/hearts.php'; ?>
        <p class="title">{ title summarizing year }</p>
      </div>
      <div class="readmore">
        <div class="marker">
          <p>read more</p>
          <img class="svg" src="images/marker.svg" alt="Read More link">
        </div>
      </div>
      <div class="year-details closed">
        <p class="year">2016</p>
        <p class="title">{ title summarizing year }</p>
        <!-- <div class="divider dark"></div> -->
        <?php include 'includes/views/hearts.php'; ?>
        <p class="writeup">Phasellus aliquet augue mauris, eu sollicitudin arcu fringilla sed. Quisque eu massa semper, gravida erat eget, dignissim est. Curabitur sed velit mauris. Nullam vitae fringilla nisl. Sed finibus neque et erat iaculis, at euismod urna pretium. Praesent ultricies nulla leo, eget hendrerit velit luctus eget.<br><br>Etiam diam nisi, convallis a dapibus quis, gravida ac dui. Ut laoreet dolor odio, et mattis nibh auctor eu. Aliquam nec facilisis massa, a mollis eros. Quisque vestibulum mauris at lacus placerat, eget egestas quam ullamcorper. Sed ac ligula mauris. Suspendisse convallis, urna ac ullamcorper consectetur, quam arcu tempus orci, ut condimentum justo enim eu nibh. ficitur laoreet.</p>
        <div class="close">X</div>
      </div>
      <!-- cake -->
      <!-- <img src="images/temp/pexels/pexels-photo-613056.jpeg" class="background"> -->
      <div style="background-image: url(<?=$images?>pexels-photo-613056.jpeg)" class="background"></div>
    </div>
    <div class="slide">
      <div class="title-header">
        <h3 class="story-year">2017<span class="hidden"> | { year title }</span></h3>
        <!-- <div class="divider light"></div> -->
        <?php include 'includes/views/hearts.php'; ?>
        <p class="title">{ title summarizing year }</p>
      </div>
      <div class="readmore">
        <div class="marker">
          <p>read more</p>
          <img class="svg" src="images/marker.svg" alt="Read More link">
        </div>
      </div>
      <div class="year-details closed">
        <p class="year">2017</p>
        <p class="title">{ title summarizing year }</p>
        <!-- <div class="divider dark"></div> -->
        <?php include 'includes/views/hearts.php'; ?>
        <p class="writeup">Phasellus aliquet augue mauris, eu sollicitudin arcu fringilla sed. Quisque eu massa semper, gravida erat eget, dignissim est. Curabitur sed velit mauris. Nullam vitae fringilla nisl. Sed finibus neque et erat iaculis, at euismod urna pretium. Praesent ultricies nulla leo, eget hendrerit velit luctus eget.<br><br>Etiam diam nisi, convallis a dapibus quis, gravida ac dui. Ut laoreet dolor odio, et mattis nibh auctor eu. Aliquam nec facilisis massa, a mollis eros. Quisque vestibulum mauris at lacus placerat, eget egestas quam ullamcorper. Sed ac ligula mauris. Suspendisse convallis, urna ac ullamcorper consectetur, quam arcu tempus orci, ut condimentum justo enim eu nibh. ficitur laoreet.</p>
        <div class="close">X</div>
      </div>
      <!-- engaged -->
      <!-- <img src="images/temp/pexels/pexels-photo-789343.jpeg" class="background"> -->
      <div style="background-image: url(<?=$images?>pexels-photo-789343.jpeg)" class="background"></div>
    </div>
  </div>
</section>