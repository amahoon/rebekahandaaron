<section class="registry" id="registry">
  <div class="title">
    <h2>&lt;Registry&gt;</h2>
    <!-- <div class="divider dark"></div> -->
    <?php include 'includes/views/hearts.php'; ?>
  </div>
  <p>We love every one of you deeply and are honoured that you would like to bless us with gifts to start our new life together. We have registered and we ask that you take a look at the list first for anything that you may like to purchase. For our honeymoon we are planning on going on a cross country road trip across Canada. If you’d prefer to help us with this trip we are accepting cash gifts as well, any amount is greatly appreciated.</p>
  <a href="https://www.myregistry.com/wedding-registry/Rebekah-Hawkins-and-Aaron-Schiffert-Dunchurch-ONTARIO/1517446" target="_blank"><img src="images/MyRegistry-logo.png" alt="MyRegistry.com logo"></a>
</section>