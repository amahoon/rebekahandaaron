<section class="rsvp" id="rsvp">
  <div class="title">
    <h2>&lt;RSVP&gt;</h2>
    <!-- <div class="divider dark"></div> -->
    <?php include 'includes/views/hearts.php'; ?>
  </div>
  <div id="messages"></div>
  <form onsubmit="return validateForm()" method="post" accept-charset="utf-8">
    <div class="sidebar side-left">
      <div class="field field-text-field field-name--fullname required">
        <label class="field-label">
          <p>your full name</p>
          <input type="text" name="fullname" autocomplete="off" required pattern="^(\b[A-Z]\w*\s*)+$">
          <span class="hint">Each word of the entered name must be capitalized</span>
        </label>
      </div>
      <div class="field field-text-field field-name--email required">
        <label class="field-label">
          <p>your email</p>
          <input type="text" name="email" autocomplete="off" required>
          <span class="hint email">why do we need this?</span>
        </label>
        <p class="whyweneedit">We need your email to verify if you have already RSVPed to our wedding and in such a case that an emergency occurs and we need to contact you regarding the wedding (ex. location or time change). We promise to NOT sell or provide your email to any third party services.</p>
      </div>
      <div class="field field-radios field-name--attending required">
        <label class="field-label"><p>will you be attending?</p></label>
        <label class="radio-option"><input type="radio" name="attending" value="1" checked required><span class="checkmark"></span><p>yes</p></label>
        <label class="radio-option"><input type="radio" name="attending" value="0" required><span class="checkmark"></span><p>no</p></label>
      </div>
      <div class="field field-radios field-name--guest required">
        <label class="field-label"><p>bringing a guest?</p></label>
        <label class="radio-option"><input type="radio" name="guest" value="1" required><span class="checkmark"></span><p>yes</p></label>
        <label class="radio-option"><input type="radio" name="guest" value="0" checked required><span class="checkmark"></span><p>no</p></label>
      </div>
      <div class="field field-text-field field-name--guest-fullname disabled">
        <label class="field-label">
          <p>guest's full name</p>
          <input type="text" name="guest-fullname" autocomplete="off" disabled pattern="^(\b[A-Z]\w*\s*)+$">
          <span class="hint">Each word of the entered name must be capitalized</span>
        </label>
      </div>
      <div class="field field-textarea field-name--comments">
        <label class="field-label">
          <p>additional comments<br><span class="hint">If you have dietary restrictions or want to make a song request, please list them here.</span></p>
          <textarea name="comments" rows="6" autocomplete="off" maxlength="450"></textarea>
          <p class="wordcount"><span class="count">0</span> / <span class="max">0</span></p>
        </label>
      </div>
    </div>
    <div class="sidebar side-right">
      <div class="field field-password-field field-name--password required">
        <label class="field-label">
          <p>password</p>
          <input type="password" name="password" autocomplete="new-password" required>
        </label>
      </div>
      <div class="field field-submit">
        <input type="text" name="fname" id="robots" class="hidden">
        <input type="submit" name="submit" value="SUBMIT">
        <span class="hint">You have already submitted your RSVP, thank you!</span>
      </div>
      <!-- <div class="divider dark"></div> -->
      <?php include 'includes/views/hearts.php'; ?>
      <div class="meal-options">
        <p class="invitation">We will be having a buffet style barbeque where you will be able to choose whatever you’d like to eat for dinner right off the grill. There will be chicken, salmon, or steak as well as vegetarian options and an open bar. We hope to see you there!</p>
        <!-- <ul>
          <li class="first"><img src="images/temp/chicken-icon.png" alt="icon"></li>
          <li><img src="images/temp/fish-icon.png" alt="icon"></li>
          <li><img src="images/temp/beef-icon.png" alt="icon"></li>
          <li class="last"><img src="images/temp/vegetarian-icon.png" alt="icon"></li>
        </ul> -->
        <p class="anyquestions">If you have any questions, please email: <a href="mailto:<?=$email?>"><?=$email?></a></p>
      </div>
    </div>
  </form>
  <!-- <p class="anyquestions">if you have any questions, please email: <a href="mailto:<?=$email?>"><?=$email?></a></p> -->
</section>