<section class="bridalparty" id="bridalparty">
  <div class="title">
    <h2 class="hidden">Bridal Party</h2>
  </div>
  <?php include 'bridalparty/bridesmaids.php'; ?>
  <?php include 'bridalparty/groomsmen.php'; ?>
</section>