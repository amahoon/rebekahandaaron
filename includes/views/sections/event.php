<section class="event" id="event">
  <div class="title">
    <h2>&lt;Event&gt;</h2>
    <!-- <div class="divider dark"></div> -->
    <?php include 'includes/views/hearts.php'; ?>
  </div>
  <div class="main-content">
    <div class="sidebar side-left">
      <?php include 'event/calendar.php'; ?>
    </div>
    <div class="sidebar side-right">
      <div id="wedding-date">Saturday, Septermber 22, 2018</div>
      <?php include 'event/countdown.php'; ?>
      <div class="details">
        <div class="location">
          <p>Island Yacht Club<br>2 Muggs Island Park<br>Toronto,ON</p>
        </div>
        <!-- <div class="divider dark"></div> -->
        <?php include 'includes/views/hearts.php'; ?>
        <div class="information">
          <p>Mauris tincidunt urna eu lacinia varius. Nam eget tristique dolor. Sed venenatis consectetur diam vitae maximus. In a est et libero tempor facilisis ut sed purus. Donec neque massa, hendrerit vitae faucibus eget, sagittis nec nibh. Donec ut ultrices diam. Etiam quis volutpat est. Etiam sodales dolor a vehicula venenatis.</p>
        </div>
      </div>
    </div>
    <?php // include 'event/hotels.php'; ?>
  </div>
</section>