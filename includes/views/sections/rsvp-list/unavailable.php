<?php

  include '../config.php';

  $invitees = new Invitees();
  $unavailable = $invitees->getUnavailable();

?>
  
<div class="rsvp--unavailable">
  <div class="title">
    <h3>&lt;Unavailable&gt;</h3>
    <?php include 'includes/views/hearts.php'; ?>
  </div>
  <?php if (!empty($unavailable)) { ?>
    <ul>
      <?php foreach ($unavailable as $row) { ?>
        <?php $attending = ($row['attending']) ? 'Yes' : 'No'; ?>
        <?php $guest = (!is_null($row['guest_name'])) ? 'Yes' : 'No'; ?>
        <li>
          <div class="name">
            <p class="fullname"><?=$row['fullname']?></p>
          </div>
          <div class="info">
            <p class="email"><span class="label">Email:</span> <a href="mailto:<?=$row['email']?>"><?=$row['email']?></a></p>
            <p class="attending"><span class="label">Is <?=$row['fullname']?> coming?</span> <?=$attending?></p>
          </div>
        </li>
      <?php } ?>
    </ul>
  <?php } else { ?>
    <div class="no-results">
      <p>There are currently no invitees that have submitted an RSVP as <span class="label">unavailable</span>.</p>
    </div>
  <?php }?>
</div>