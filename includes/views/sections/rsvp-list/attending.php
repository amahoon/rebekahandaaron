<?php

  include '../config.php';

  $invitees = new Invitees();
  $attending = $invitees->getAttending();

?>
  
<div class="rsvp--attending">
  <div class="title">
    <h3>&lt;Attending&gt;</h3>
    <?php include 'includes/views/hearts.php'; ?>
  </div>
  <?php if (!empty($attending)) { ?>
    <ul>
      <?php foreach ($attending as $row) { ?>
        <?php $attending = ($row['attending']) ? 'Yes' : 'No'; ?>
        <?php $guest = (!is_null($row['guest_name'])) ? 'Yes' : 'No'; ?>
        <li>
          <div class="name">
            <p class="fullname"><?=$row['fullname']?></p>
          </div>
          <div class="info">
            <p class="email"><span class="label">Email:</span> <a href="mailto:<?=$row['email']?>"><?=$row['email']?></a></p>
            <p class="attending"><span class="label">Is <?=$row['fullname']?> coming?</span> <?=$attending?></p>
            <p class="guest"><span class="label">Is <?=$row['fullname']?> bringing a guest?</span> <?=$guest?></p>
            <?php if (!is_null($row['guest_name'])) { ?>
              <p class="guest"><span class="label"><?=$row['fullname']?> is bringing:</span> <?=$row['guest_name']?></p>
            <?php } ?>
          </div>
        </li>
      <?php } ?>
    </ul>
  <?php } else { ?>
    <div class="no-results">
      <p>There are currently no invitees that have submitted an RSVP as <span class="label">attending</span>.</p>
    </div>
  <?php }?>
</div>