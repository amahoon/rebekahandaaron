<?php

  include '../config.php';

  $invitees = new Invitees();
  $guests = $invitees->getGuests();

?>
  
<div class="rsvp--guests">
  <div class="title">
    <h3>&lt;Guests&gt;</h3>
    <?php include 'includes/views/hearts.php'; ?>
  </div>
  <?php if (!empty($guests)) { ?>
    <ul>
      <?php foreach ($guests as $row) { ?>
        <?php $attending = ($row['attending']) ? 'Yes' : 'No'; ?>
        <?php $guest = (!is_null($row['guest_name'])) ? 'Yes' : 'No'; ?>
        <li>
          <div class="name">
            <p class="fullname"><?=$row['guest_name']?></p>
          </div>
          <div class="info">
            <p class="invitee"><span class="label">Invited by:</span> <?=$row['fullname']?></p>
            <p class="email"><span class="label">Email <?=$row['fullname']?>:</span> <a href="mailto:<?=$row['email']?>"><?=$row['email']?></a></p>
          </div>
        </li>
      <?php } ?>
    </ul>
  <?php } else { ?>
    <div class="no-results">
      <p>There are currently no <span class="label">guests</span> invited to the wedding.</p>
    </div>
  <?php }?>
</div>