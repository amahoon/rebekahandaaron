<div class="countdown">
  <div class="container">
    <div id="months" class="time-container">
      <div class="digits">
        <div class="digit digit--1"><p>0</p></div>
        <div class="digit digit--2 last"><p>5</p></div>
      </div>
      <label>Months</label>
    </div>
    <div id="days" class="time-container">
      <div class="digits">
        <div class="digit digit--1"><p>0</p></div>
        <div class="digit digit--2 last"><p>4</p></div>
      </div>
      <label>Days</label>
    </div>
    <div id="hours" class="time-container">
      <div class="digits">
        <div class="digit digit--1"><p>1</p></div>
        <div class="digit digit--2 last"><p>3</p></div>
      </div>
      <label>Hours</label>
    </div>
    <div id="mins" class="time-container last">
      <div class="digits">
        <div class="digit digit--1"><p>5</p></div>
        <div class="digit digit--2 last"><p>6</p></div>
      </div>
      <label>Minutes</label>
    </div>
  </div>
</div>