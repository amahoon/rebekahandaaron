<section class="gallery" id="gallery">
  <h2 class="hidden">Gallery</h2>
  <div class="container m-p-g">

    <div class="m-p-g__thumbs" data-google-image-layout data-max-height="250">
      <img src="images/temp/pexels/pexels-photo-40525.jpeg" data-full="images/temp/pexels/pexels-photo-40525.jpeg" class="m-p-g__thumbs-img">
      <img src="images/temp/pexels/pexels-photo-460823.jpeg" data-full="images/temp/pexels/pexels-photo-460823.jpeg" class="m-p-g__thumbs-img">
      <img src="images/temp/pexels/pexels-photo-613056.jpeg" data-full="images/temp/pexels/pexels-photo-613056.jpeg" class="m-p-g__thumbs-img">
      <img src="images/temp/pexels/pexels-photo-704557.jpeg" data-full="images/temp/pexels/pexels-photo-704557.jpeg" class="m-p-g__thumbs-img">
      <img src="images/temp/pexels/pexels-photo-789343.jpeg" data-full="images/temp/pexels/pexels-photo-789343.jpeg" class="m-p-g__thumbs-img">
      <img src="images/temp/pexels/pexels-photo-789786.jpeg" data-full="images/temp/pexels/pexels-photo-789786.jpeg" class="m-p-g__thumbs-img">
      <img src="images/temp/pexels/pexels-photo-895231.jpeg" data-full="images/temp/pexels/pexels-photo-895231.jpeg" class="m-p-g__thumbs-img">
      <img src="images/temp/pexels/pexels-photo-196666.jpeg" data-full="images/temp/pexels/pexels-photo-196666.jpeg" class="m-p-g__thumbs-img">
      <img src="images/temp/pexels/pexels-photo-247858.jpeg" data-full="images/temp/pexels/pexels-photo-247858.jpeg" class="m-p-g__thumbs-img">
      <img src="images/temp/pexels/pexels-photo-265730.jpeg" data-full="images/temp/pexels/pexels-photo-265730.jpeg" class="m-p-g__thumbs-img">
      <img src="images/temp/pexels/pexels-photo-540522.jpeg" data-full="images/temp/pexels/pexels-photo-540522.jpeg" class="m-p-g__thumbs-img">
      <img src="images/temp/pexels/pexels-photo-794254.jpeg" data-full="images/temp/pexels/pexels-photo-794254.jpeg" class="m-p-g__thumbs-img">
    </div>
 
    <div class="m-p-g__fullscreen"></div>
  </div>
</section>