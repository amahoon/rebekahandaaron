<div class="bridesmaids">
  <div class="title">
    <h3>&lt;Bridesmaids&gt;</h3>
    <!-- <div class="divider dark"></div> -->
    <?php include 'includes/views/hearts.php'; ?>
  </div>
  <div class="slick-carousel bridesmaids">
    <div class="slide">
      <div class="profilepic">
        <div class="picture">
          <img class="svg" src="images/avatars/alyssa/myAvatar.svg" alt="Cartoonized Profile Picture">
        </div>
      </div>
      <div class="party--information">
        <div class="name-and-title">
          <img class="svg svg--drop" src="images/drop-outline.svg" alt="Drop indicator">
          <h4>Alyssa Mahon</h4>
          <p class="subtitle">Maid of Honour &amp; best friend</p>
        </div>
        <div class="quote">
          <img class="svg svg--quotes-start" src="images/quotes.svg" alt="Cartoonized Profile Picture">
          <p>Duis sed mattis mauris, eu finibus odio. Mauris at volutpat velit. Phasellus a nibh consectetur, facilisis lectus vitae, porttitor augue. Donec elementum euismod velit, quis fermentum sem pretium quis. Quisque a pellentesque turpis, nec auctor magna. Nam nec arcu libero.</p>
          <img class="svg svg--quotes-end" src="images/quotes.svg" alt="Cartoonized Profile Picture">
        </div>
      </div>
    </div>
    <div class="slide">
      <div class="profilepic">
        <div class="picture">
          <img class="svg" src="images/avatars/michaella/myAvatar.svg" alt="Cartoonized Profile Picture">
        </div>
      </div>
      <div class="party--information">
        <div class="name-and-title">
          <img class="svg svg--drop" src="images/drop-outline.svg" alt="Drop indicator">
          <h4>Michaella Hawkins</h4>
          <p class="subtitle">Bridesmaid &amp; sister</p>
        </div>
        <div class="quote">
          <img class="svg svg--quotes-start" src="images/quotes.svg" alt="Cartoonized Profile Picture">
          <p>Duis sed mattis mauris, eu finibus odio. Mauris at volutpat velit. Phasellus a nibh consectetur, facilisis lectus vitae, porttitor augue. Donec elementum euismod velit, quis fermentum sem pretium quis. Quisque a pellentesque turpis, nec auctor magna. Nam nec arcu libero.</p>
          <img class="svg svg--quotes-end" src="images/quotes.svg" alt="Cartoonized Profile Picture">
        </div>
      </div>
    </div>
    <div class="slide">
      <div class="profilepic">
        <div class="picture">
          <img class="svg" src="images/avatars/melissa/myAvatar.svg" alt="Cartoonized Profile Picture">
        </div>
      </div>
      <div class="party--information">
        <div class="name-and-title">
          <img class="svg svg--drop" src="images/drop-outline.svg" alt="Drop indicator">
          <h4>Melissa Little</h4>
          <p class="subtitle">Bridesmaid &amp; friend</p>
        </div>
        <div class="quote">
          <img class="svg svg--quotes-start" src="images/quotes.svg" alt="Cartoonized Profile Picture">
          <p>Duis sed mattis mauris, eu finibus odio. Mauris at volutpat velit. Phasellus a nibh consectetur, facilisis lectus vitae, porttitor augue. Donec elementum euismod velit, quis fermentum sem pretium quis. Quisque a pellentesque turpis, nec auctor magna. Nam nec arcu libero.</p>
          <img class="svg svg--quotes-end" src="images/quotes.svg" alt="Cartoonized Profile Picture">
        </div>
      </div>
    </div>
    <div class="slide">
      <div class="profilepic">
        <div class="picture">
          <img class="svg" src="images/avatars/krystal/myAvatar.svg" alt="Cartoonized Profile Picture">
        </div>
      </div>
      <div class="party--information">
        <div class="name-and-title">
          <img class="svg svg--drop" src="images/drop-outline.svg" alt="Drop indicator">
          <h4>Krystal { last name }</h4>
          <p class="subtitle">Bridesmaid &amp; friend</p>
        </div>
        <div class="quote">
          <img class="svg svg--quotes-start" src="images/quotes.svg" alt="Cartoonized Profile Picture">
          <p>Duis sed mattis mauris, eu finibus odio. Mauris at volutpat velit. Phasellus a nibh consectetur, facilisis lectus vitae, porttitor augue. Donec elementum euismod velit, quis fermentum sem pretium quis. Quisque a pellentesque turpis, nec auctor magna. Nam nec arcu libero.</p>
          <img class="svg svg--quotes-end" src="images/quotes.svg" alt="Cartoonized Profile Picture">
        </div>
      </div>
    </div>
    <div class="slide">
      <div class="profilepic">
        <div class="picture">
          <img class="svg" src="images/avatars/doris/myAvatar.svg" alt="Cartoonized Profile Picture">
        </div>
      </div>
      <div class="party--information">
        <div class="name-and-title">
          <img class="svg svg--drop" src="images/drop-outline.svg" alt="Drop indicator">
          <h4>Doris { last name }</h4>
          <p class="subtitle">Bridesmaid &amp; friend</p>
        </div>
        <div class="quote">
          <img class="svg svg--quotes-start" src="images/quotes.svg" alt="Cartoonized Profile Picture">
          <p>Duis sed mattis mauris, eu finibus odio. Mauris at volutpat velit. Phasellus a nibh consectetur, facilisis lectus vitae, porttitor augue. Donec elementum euismod velit, quis fermentum sem pretium quis. Quisque a pellentesque turpis, nec auctor magna. Nam nec arcu libero.</p>
          <img class="svg svg--quotes-end" src="images/quotes.svg" alt="Cartoonized Profile Picture">
        </div>
      </div>
    </div>
  </div>
</div>