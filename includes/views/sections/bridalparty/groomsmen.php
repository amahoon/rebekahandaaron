<div class="groomsmen">
  <div class="title">
    <h3>&lt;Groomsmen&gt;</h3>
    <!-- <div class="divider dark"></div> -->
    <?php include 'includes/views/hearts.php'; ?>
  </div>
  <div class="slick-carousel groomsmen">
    <div class="slide">
      <div class="profilepic">
        <div class="picture">
          <img class="svg" src="images/avatars/shane/myAvatar.svg" alt="Cartoonized Profile Picture">
        </div>
      </div>
      <div class="party--information">
        <div class="name-and-title">
          <img class="svg svg--drop" src="images/drop-outline.svg" alt="Drop indicator">
          <h4>Shane Holmer</h4>
          <p class="subtitle">Best Man &amp; brother</p>
        </div>
        <div class="quote">
          <img class="svg svg--quotes-start" src="images/quotes.svg" alt="Cartoonized Profile Picture">
          <p>Sed suscipit, nulla eu suscipit faucibus, justo ipsum ornare dolor, nec tempus sapien tortor non tortor. Nunc posuere sollicitudin libero, ac bibendum justo porttitor dapibus. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Mauris pellentesque cursus.</p>
          <img class="svg svg--quotes-end" src="images/quotes.svg" alt="Cartoonized Profile Picture">
        </div>
      </div>
    </div>
    <div class="slide">
      <div class="profilepic">
        <div class="picture">
          <img class="svg" src="images/avatars/shane/myAvatar.svg" alt="Cartoonized Profile Picture">
        </div>
      </div>
      <div class="party--information">
        <div class="name-and-title">
          <img class="svg svg--drop" src="images/drop-outline.svg" alt="Drop indicator">
          <h4>Billy { last name }</h4>
          <p class="subtitle">Groomsman &amp; friend</p>
        </div>
        <div class="quote">
          <img class="svg svg--quotes-start" src="images/quotes.svg" alt="Cartoonized Profile Picture">
          <p>Sed suscipit, nulla eu suscipit faucibus, justo ipsum ornare dolor, nec tempus sapien tortor non tortor. Nunc posuere sollicitudin libero, ac bibendum justo porttitor dapibus. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Mauris pellentesque cursus.</p>
          <img class="svg svg--quotes-end" src="images/quotes.svg" alt="Cartoonized Profile Picture">
        </div>
      </div>
    </div>
    <div class="slide">
      <div class="profilepic">
        <div class="picture">
          <img class="svg" src="images/avatars/shane/myAvatar.svg" alt="Cartoonized Profile Picture">
        </div>
      </div>
      <div class="party--information">
        <div class="name-and-title">
          <img class="svg svg--drop" src="images/drop-outline.svg" alt="Drop indicator">
          <h4>Booby { last name }</h4>
          <p class="subtitle">Groomsman &amp; friend</p>
        </div>
        <div class="quote">
          <img class="svg svg--quotes-start" src="images/quotes.svg" alt="Cartoonized Profile Picture">
          <p>Sed suscipit, nulla eu suscipit faucibus, justo ipsum ornare dolor, nec tempus sapien tortor non tortor. Nunc posuere sollicitudin libero, ac bibendum justo porttitor dapibus. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Mauris pellentesque cursus.</p>
          <img class="svg svg--quotes-end" src="images/quotes.svg" alt="Cartoonized Profile Picture">
        </div>
      </div>
    </div>
    <div class="slide">
      <div class="profilepic">
        <div class="picture">
          <img class="svg" src="images/avatars/shane/myAvatar.svg" alt="Cartoonized Profile Picture">
        </div>
      </div>
      <div class="party--information">
        <div class="name-and-title">
          <img class="svg svg--drop" src="images/drop-outline.svg" alt="Drop indicator">
          <h4>Tyler { last name }</h4>
          <p class="subtitle">Groomsman &amp; friend</p>
        </div>
        <div class="quote">
          <img class="svg svg--quotes-start" src="images/quotes.svg" alt="Cartoonized Profile Picture">
          <p>Sed suscipit, nulla eu suscipit faucibus, justo ipsum ornare dolor, nec tempus sapien tortor non tortor. Nunc posuere sollicitudin libero, ac bibendum justo porttitor dapibus. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Mauris pellentesque cursus.</p>
          <img class="svg svg--quotes-end" src="images/quotes.svg" alt="Cartoonized Profile Picture">
        </div>
      </div>
    </div>
    <div class="slide">
      <div class="profilepic">
        <div class="picture">
          <img class="svg" src="images/avatars/shane/myAvatar.svg" alt="Cartoonized Profile Picture">
        </div>
      </div>
      <div class="party--information">
        <div class="name-and-title">
          <img class="svg svg--drop" src="images/drop-outline.svg" alt="Drop indicator">
          <h4>{ first name } { last name }</h4>
          <p class="subtitle">Groomsman &amp; { relationship to groom }</p>
        </div>
        <div class="quote">
          <img class="svg svg--quotes-start" src="images/quotes.svg" alt="Cartoonized Profile Picture">
          <p>Sed suscipit, nulla eu suscipit faucibus, justo ipsum ornare dolor, nec tempus sapien tortor non tortor. Nunc posuere sollicitudin libero, ac bibendum justo porttitor dapibus. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Mauris pellentesque cursus.</p>
          <img class="svg svg--quotes-end" src="images/quotes.svg" alt="Cartoonized Profile Picture">
        </div>
      </div>
    </div>
  </div>
</div>