<?php
  // $base_url = "$_SERVER[DOCUMENT_ROOT]";
  // $base_url = (!empty($_SERVER['HTTPS']) ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'] . '/';
  // $base_url = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
  include '../../config.php';

  $filters = $_GET;

  $password = $_GET['password'];
  $password_check = new PasswordCheck();
  $valid = $password_check->checkPassword($password);

  if ($valid) {
    $invitees = new Invitees();
    $results = $invitees->get($filters);
    echo json_encode($results);
  } else {
    echo '{ "password":"invalid"}';
  }