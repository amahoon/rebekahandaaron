<!-- FIX HEADER : LOGO GETS HUGE ON MOBILE AND FORCES TO TOP WHEN CLICKING MENU ICON -->

<header>
  <h1 class="hidden">Rebekah and Aaron are Getting Married</h1>
  <div class="mobilemenu">
    <div class="head">
      <div class="mobilemenu-controller"></div>
      <nav class="main-navigation">
        <h2 class="hidden">Main Navigation</h2>
        <ul>
          <li><h3 class="hidden">Our Story</h3><p class="ourstory">Our Story</p></li>
          <li><h3 class="hidden">Event</h3><p class="event">Event</p></li>
          <li><h3 class="hidden">Bridal Party</h3><p class="bridalparty">Bridal Party</p></li>
          <!-- <li class="logo"><img src="images/logo.png" alt="Rebekah and Aaron Logo"></li> -->
          <li class="logo"><img src="images/logo2.png" alt="Rebekah and Aaron Logo"></li>
          <li><h3 class="hidden">RSVP</h3><p class="rsvp">RSVP</p></li>
          <li><h3 class="hidden">Registry</h3><p class="registry">Registry</p></li>
          <li><h3 class="hidden">Gallery</h3><p class="gallery">Gallery</p></li>
        </ul>
      </nav>
    </div>
  </div>
</header>