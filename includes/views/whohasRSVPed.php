<?php
  
  include '../config.php';

?>

<section class="rsvp-list">
  <div class="container">  
    <?php include 'sections/rsvp-list/attending.php'; ?>
    <?php include 'sections/rsvp-list/unavailable.php'; ?>
    <?php include 'sections/rsvp-list/guests.php'; ?>
  </div>
</section>