<div id="loader">
  <div id="particles-background" class="vertical-centered-box"></div>
  <!-- <div id="particles-foreground" class="vertical-centered-box"></div> -->

  <div id="particles-foreground" class="vertical-centered-box"></div>

  <div class="vertical-centered-box">
    <div class="content">
      <div class="loader-circle"></div>
      <div class="loader-line-mask">
        <div class="loader-line"></div>
      </div>
      <img class="svg loader-logo" src="images/mahon-logo.svg" alt="Alyssa Mahon's logo">
    </div>
  </div>
</div>