<footer>
  <div class="credits">
    <div class="copyright">
      <p>copyright &copy; 2018</p>
    </div>
    <div class="madeby">
      <p>designed and developed by <a href="www.linkedin.com/in/alyssa-mahon" target="_blank">Alyssa Mahon <img class="svg" src="images/mahon-logo.svg" alt="Alyssa Mahon's logo"></a></p>
    </div>
  </div>
</footer>