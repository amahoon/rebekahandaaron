<?php
  
  include '../config.php';

  $invitees = new Invitees();
  $guests = $invitees->getGuests();
  $available = $invitees->getAttending();
  $unavailable = $invitees->getUnavailable();

  $g_count = count($guests);
  $a_count = count($available);
  $ua_count = count($unavailable);
?>

<div class="guest-count">
  <div class="list">
    <ul>
      <li><span class="count"><?=$a_count?></span><br><span class="label">Attending</span></li>
      <li><span class="count"><?=$ua_count?></span><br><span class="label">Unavailable</span></li>
      <li><span class="count"><?=$g_count?></span><br><span class="label">Guests</span></li>
    </ul>
  </div>
</div>