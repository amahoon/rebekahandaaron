<?php

/**
 * @file
 * Creates an object to check the user entered password to RSVP.
 */

class PasswordCheck {

  protected $hash;

  public function __construct() {
    $this->hash = password_hash('08032016', PASSWORD_DEFAULT);
  }

  public function connect() {
    return new Connection;
  }

  public function gethash() {
    return $this->hash;
  }

  public function checkPassword($value) {
    if (password_verify($value, $this->hash)) {
      return TRUE;
    } else {
      return FALSE;
    }
  }

}