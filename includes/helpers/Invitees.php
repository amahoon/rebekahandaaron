<?php

/**
 * @file
 * Creates an object with method calls related to invitees.
 */

class Invitees {

  public function connect() {
    return new Connection;
  }

  public function get($filters) {
    $connection = $this->connect();

    $connection->select('invitee','i');
    $connection->leftJoin('guest','g','g.invitee_id = i.id');
    $connection->fields('i');
    $connection->addField('g','fullname','guest_name');
    $connection->orderBy('fullname');
    $connection->groupBy('i.id');

    foreach ($filters as $name => $value) {
      if ($name == 'email') {
        $connection->condition('i.' . $name,$value);
      }
    }

    $res = $connection->execute();
    $results = $connection->arrayResults($res);

    return $results;
  }

  public function getAll() {
    $connection = $this->connect();

    $connection->select('invitee','i');
    $connection->leftJoin('guest','g','g.invitee_id = i.id');
    $connection->fields('i');
    $connection->addField('g','fullname','guest_name');
    $connection->orderBy('fullname');
    $connection->groupBy('i.id');

    $res = $connection->execute();
    $results = $connection->arrayResults($res);

    return $results;
  }

  public function getAttending() {
    $connection = $this->connect();

    $connection->select('invitee','i');
    $connection->leftJoin('guest','g','g.invitee_id = i.id');
    $connection->fields('i');
    $connection->addField('g','fullname','guest_name');
    $connection->orderBy('fullname');
    $connection->groupBy('i.id');
    
    $connection->condition('i.attending','1');

    $res = $connection->execute();
    $results = $connection->arrayResults($res);

    return $results;
  }

  public function getUnavailable() {
    $connection = $this->connect();

    $connection->select('invitee','i');
    $connection->fields('i');
    $connection->orderBy('fullname');
    $connection->groupBy('i.id');
    $connection->condition('i.attending','0');

    $res = $connection->execute();
    $results = $connection->arrayResults($res);

    return $results;
  }

  public function getGuests() {
    $connection = $this->connect();

    $connection->select('guest','g');
    $connection->leftJoin('invitee','i','g.invitee_id = i.id');
    $connection->fields('i');
    $connection->addField('g','fullname','guest_name');
    $connection->orderBy('guest_name');
    $connection->groupBy('g.id');

    $res = $connection->execute();
    $results = $connection->arrayResults($res);

    return $results;
  }

  public function insert($vals) {
    $fields = array();
    $values = array();
    foreach ($vals as $field => $value) {
      $val = '"' . $value . '"';
      if (strpos($field,'guest') !== 0) {
        $fields['invitee'][$field] = $field;
        $values['invitee'][$field] = $val;
      } else {
        $fields['guest'][str_replace('guest-','',$field)] = str_replace('guest-','',$field);
        $values['guest'][str_replace('guest-','',$field)] = $val;
      }
    }
    $connection = $this->connect();
    $id = $connection->insert('invitee', $fields['invitee'], $values['invitee']);

    foreach ($values['guest'] as $field => $value) {
      if ($field == 'guest' && $value) {
        $guest_fields = $fields['guest'];
        $invitee_id = array('invitee_id' => 'invitee_id');
        $guest_fields = $invitee_id + $guest_fields;

        $guest_values = $values['guest'];
        $invitee_id = array('invitee_id' => $id);
        $guest_values = $invitee_id + $guest_values;
        unset($guest_fields['guest']);
        unset($guest_values['guest']);

        // echo '<div style="background-color: white;"><pre>' . var_export($guest_fields,TRUE) . '</pre></div>';
        // echo '<div style="background-color: white;"><pre>' . var_export($guest_values,TRUE) . '</pre></div>';

        $gid = $connection->insert('guest', $guest_fields, $guest_values);
        break;
      }
    }
  }

}