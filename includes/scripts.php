<!-- LOADER IS IN INDEX.PHP -->

<!-- node_modules -->
<!-- <script type="text/javascript" src="node_modules/jquery/dist/jquery.min.js"></script> -->
<!-- <script type="text/javascript" src="node_modules/jquery-match-height/jquery.matchHeight.js"></script> -->
<script type="text/javascript" src="node_modules/slick-carousel/slick/slick.min.js"></script>
<script type="text/javascript" src="node_modules/material-photo-gallery/dist/js/material-photo-gallery.min.js"></script>

<!-- vendor -->
<script type="text/javascript" src="dist/js/vendor/jQuery-Flex-Vertical-Center-master/jquery.flexverticalcenter.min.js" defer></script>

<!-- external -->
<script type="text/javascript" src="https://cdn.rawgit.com/nizarmah/calendar-javascript-lib/master/calendarorganizer.min.js"></script>
<script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.3/moment.js"></script>

<!-- custom javascript -->
<script type="text/javascript" src="dist/js/countdown.min.js"></script>
<script type="text/javascript" src="dist/js/browser.min.js"></script>
<script type="text/javascript" src="dist/js/calendar.min.js"></script>
<!-- <script type="text/javascript" src="dist/js/places.min.js"></script> -->
<script type="text/javascript" src="dist/js/misc.min.js"></script>
<script type="text/javascript" src="dist/js/rsvp.min.js"></script>
<script type="text/javascript" src="dist/js/story.min.js"></script>
<script type="text/javascript" src="dist/js/gallery.min.js"></script>
<script type="text/javascript" src="dist/js/navigation.min.js"></script>

<!-- Google Map -->
<?php $maps_key = 'AIzaSyDJebnxPiIO3UMaWRsnOQbOSpp9r7Z3o44'; ?>
<script type="text/javascript" src="dist/js/googlemap.min.js" defer></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=<?=$maps_key?>&callback=initMap" defer></script>

<!-- OTHER JAVASCRIPT -->
<script>

  $(window).on('load',function() {
    $('#loader').addClass('loaded');
    $('body').removeClass('loading');
    $('html').removeClass('loading');
  });

</script>