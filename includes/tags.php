<?php

	$tags['name'] = 'Enhanced Medical Nutrition';
	$tags['abbrev'] = 'EMN';
	$tags['title'] = $tags['abbrev'] . ' &#124; ' . $tags['name'];
	$tags['desc'] = 'Quisque id mi. Praesent ac sem eget est egestas volutpat. Vestibulum turpis sem, aliquet eget, lobortis pellentesque, rutrum eu, nisl. Fusce pharetra convallis urna. Nullam nulla eros, ultricies sit amet, nonummy id, imperdiet feugiat, pede. Vivamus in erat ut urna cursus vestibulum. Vestibulum purus quam, scelerisque ut, mollis sed, nonummy id, metus. Donec interdum, metus et hendrerit aliquet, dolor diam sagittis ligula, eget egestas libero turpis vel mi. Aenean commodo ligula eget dolor. Donec orci lectus, aliquam ut, faucibus non, euismod id, nulla.';
	$tags['keywords'] = 'health medical nutrition';
	$tags['author'] = 'Alyssa Mahon';
	$tags['url'] = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
	$tags['image'] = $tags['url'] . '/images/logos/emn-logo-2.png';