<?php 

  $base_url = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
  // $base_url = "$_SERVER[DOCUMENT_ROOT]$_SERVER[REQUEST_URI]";
  $email = 'bride@rebekahandaaron.ca';
  
  $includes = array();
  if (strpos($base_url,'localhost:8888') !== 0) {
    $root = 'includes/';
    $includes[] = $root . 'helpers/Debug.php';
  } else {
    $root = '';
  }
  $includes[] = $root . 'helpers/Connection.php';
  $includes[] = $root . 'helpers/Invitees.php';
  $includes[] = $root . 'helpers/PasswordCheck.php';

  $base_url = '/Applications/MAMP/htdocs/rebekahandaaron/';
	foreach ($includes as $file) {
  	if (!in_array($base_url . $file,get_included_files())) {
      include $base_url . $file;
    }
	}

  // DEBUG EXAMPLE
  // $debug = new Debug();
  // $debug->dpm($base_url);