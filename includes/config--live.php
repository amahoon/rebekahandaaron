<?php 

  // $url = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
  $url = "$_SERVER[DOCUMENT_ROOT]$_SERVER[REQUEST_URI]";
  $base_url = substr($url, 0, strpos($url,'includes/'));
  $email = 'bride@rebekahandaaron.ca';

  // echo '<div style="background-color: white;"><pre>' . var_export($base_url,TRUE) . '</pre></div>';
  
  $includes = array();
  $includes[] = 'includes/helpers/Connection.php';
  $includes[] = 'includes/helpers/Invitees.php';
  $includes[] = 'includes/helpers/PasswordCheck.php';

	foreach ($includes as $file) {
  	if (!in_array($base_url . $file,get_included_files())) {
      include $base_url . $file;
    }
	}

  // DEBUG EXAMPLE
  // $debug = new Debug();
  // $debug->dpm($base_url);