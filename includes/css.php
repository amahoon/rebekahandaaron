<!-- node_modules -->
<link href="node_modules/slick-carousel/slick/slick.css" rel="stylesheet" type="text/css" media="all">
<link href="node_modules/material-photo-gallery/dist/css/material-photo-gallery.css" rel="stylesheet" type="text/css" media="all">

<!-- vendor -->
<link href="vendor/prism-loading-screen/css/style.css" rel="stylesheet prefetch" type="text/css" media="all">

<!-- external -->
<link href='https://cdn.rawgit.com/nizarmah/calendar-javascript-lib/master/calendarorganizer.min.css' rel='stylesheet' type="text/css" media="all">

<!-- custom styles -->
<link href="dist/css/styles.css" rel="stylesheet" type="text/css" media="all">