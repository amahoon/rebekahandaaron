$(document).ready(function() {
  var data = createData();

  function createData() {
    var data = createBaseData();

    // data[2018] = {};
    // data[2018][9] = {};
  
    data[2018][9][22] = [];
    data[2018][9][22].push({
      startTime: "10:00",
      endTime: "12:00",
      text: "Wedding Day"
    });

    // console.log(data);
    return data;
  }

  function createBaseData() {
    var date = new Date();
    var data = {};

    // number of years to include
    for (var i = 0; i < 10; i++) {
      data[date.getFullYear() + i] = {};

      // months in a year
      for (var j = 0; j < 12; j++) {
        data[date.getFullYear() + i][j + 1] = {};
      }
    }

    return data;
  }

  // initializing a new calendar object, that will use an html container to create itself
  var calendar = new Calendar(
    "calendarContainer", // id of html container for calendar
    "small", // size of calendar, can be small | medium | large
    [
      "Sunday", // left most day of calendar labels
      3 // maximum length of the calendar labels
    ],
    // ORIGINAL
    // [
    //   "#E91E63", // primary color
    //   "#C2185B", // primary dark color
    //   "#FFFFFF", // text color
    //   "#F8BBD0" // text dark color
    // ]
    // GREYS
    // [
    //   "#bebebe", // primary color
    //   "#9d9d9d", // primary dark color
    //   "#FFFFFF", // text color
    //   "#e1e1e1" // text dark color
    // ]
    [
      // "#2895fc", // primary color
      "#178ABF", // primary color
      // "#0100c9", // primary dark color
      "#0c4e96", // primary dark color
      "#FFFFFF", // text color
      "#e1e1e1" // text dark color
    ]
  );

  // initializing a new organizer object, that will use an html container to create itself
  var organizer = new Organizer(
    "organizerContainer", // id of html container for calendar
    calendar, // defining the calendar that the organizer is related to
    data // giving the organizer the static data that should be displayed
  );
});