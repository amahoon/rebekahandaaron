// var weddingDay = new Date('2018-04-12 12:15:00');
// var weddingDay = new Date('2018-08-03 17:00:00');
var weddingDay = new Date('2018-09-22 00:00:00');

Date.getFormattedDateDiff = function(date1, date2) {
  var b = moment(date1),
      a = moment(date2),
      intervals = ['years','months','weeks','days'],
      time = new Array(),
      countDownDate = date2.getTime(),
      now = date1.getTime(),
      distance = countDownDate - now;

  for(var i=0; i<intervals.length; i++){
    var diff = a.diff(b, intervals[i]);
    b.add(diff, intervals[i]);
    time[i] = diff;
  }

  var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
  var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
  var seconds = Math.floor((distance % (1000 * 60)) / 1000);

  intervals.push('hours');
  intervals.push('mins');
  intervals.push('seconds');

  time.push(hours);
  time.push(minutes);
  time.push(seconds);

  var months = $('#months'),
      days = $('#days'),
      hours = $('#hours'),
      mins = $('#mins');

  for(i=0;i<intervals.length;i++) {
    var element = $('#' + intervals[i]);
    if (element.length > 0) {
      var thisTime = time[i];
      var length = thisTime.toString().length,
          digit1 = 0,
          digit2 = thisTime.toString();

      if (length > 1) {
        digit1 = thisTime.toString().substring(0,1);
        digit2 = thisTime.toString().substring(1);
      }

      if (element.find('div.digit--1 p').html() != digit1) {
        element.find('div.digit--1 p').html(digit1);
      }
      if (element.find('div.digit--2 p').html() != digit2) {
        element.find('div.digit--2 p').html(digit2);
      }
    }
  }
};

$(document).ready(function() {

  setInterval(function() {
    calculateInterval();
  }, 1000);

  function calculateInterval() {
    var start = new Date(),
        end   = weddingDay;
    Date.getFormattedDateDiff(start, end);
  }
});