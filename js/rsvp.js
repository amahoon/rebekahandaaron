var form = $('section.rsvp form');
var attending = $('input[name=attending]');

var guestField = form.find('.field.field-name--guest');
var guest = $('input[name=guest]');

var guestnameField = form.find('.field.field-name--guest-fullname');
var guestname = $('input[name=guest-fullname]');

var comments = $('textarea[name=comments]');
var max = $('p.wordcount span.max');
var count = $('p.wordcount span.count');

var password = $('input[name=password]');
var robots = $('input[name=fname].hidden');
var submit = form.find('input[type=submit]');

$(document).ready(function(){
  max.html(comments.attr('maxlength'));

  attending.click(function() {
    if ($(this).is(':checked')) {
      var value = $(this).val();
      setFieldStates(guestField,guest,value);
      for (i=0;i<guest.length;i++) {
        if (guest[i].checked) {
          if (guest[i].value == 1) {
            setFieldStates(guestnameField,guestname,value);
          } else {
            setFieldStates(guestnameField,guestname,0);
          }
          break;
        }
      }
    }
  });

  comments.keyup(function(e) {
    var value = $(this).val();

    count.html(value.length);
  });

  var timer;
  comments.focusin(function(e) {
    timer = setInterval(limitCommentsLength, 500);
  });
  comments.focusout(function(e) {
    clearInterval(timer);
  });

  guest.click(function() {
    if ($(this).is(':checked')) {
      var value = $(this).val();
      setFieldStates(guestnameField,guestname,value);
    }
  });

  var why = $('.whyweneedit');
  $('.hint.email').hover(function() {
    why.addClass('visible');
  }, function() {
    why.removeClass('visible');
  });
});

function limitCommentsLength() {
  var value = comments.val();
  if (value.length > max.html()) {
    var cut = value.substring(0,max.html());
    comments.val(cut);
    count.html(cut.length);
  }
}

function setFieldStates(holder,field,value) {
  if (value == 1) {
    holder.removeClass('disabled');
    holder.addClass('required');
    field.removeAttr('disabled');
    field.attr('required','required');
  } else {
    if (holder.hasClass('required')) {
      holder.removeClass('required');
      field.removeAttr('required');
    }
    if (!holder.hasClass('disabled')) {
      holder.addClass('disabled');
      field.attr('disabled','disabled');
    }
  }
}

function getEmail() {
  var url = 'includes/email.php';
  var email = '';
  
  // Set the global configs to synchronous 
  $.ajaxSetup({
    async: false
  });

  $.getJSON(url,function(data) {
    email = data.email;
  });

  // Set the global configs back to asynchronous 
  $.ajaxSetup({
      async: true
  });

  return email;
}

function validateForm() {
  event.preventDefault();
  var url = getAjaxURL('validate');
  var input = $('form input:not(.hidden), form textarea').serialize();
  $.ajax({
    type: "POST",
    url: url,
    dataType: "json",
    data: input,
    success : function(data){
      // empty the password field so it doesn't have a value
      password.val('');
      var email = getEmail();

      if (robots.val().length > 0) {
        form.remove();

        error = '<p>Shame on you robots! Bad Wall-E!</p>';
        $('#messages').html('<div class="fail">' + error + '</div>');

        return false;
      }

      var error = '';
      if (typeof data.password !== 'undefined' && data.password == 'invalid') {
        error = '<p>The password you have entered is invalid. Please check your invitation for the correct password to complete your RSVP</p>';
      } else {
        for (i=0;i<data.length;i++) {
          error = '<p>Someone has already RSVPed with that email. If you believe this to be incorrect, please contact the bride or groom to verify your RSVP status at <a href="mailto:' + email + '">' + email + '</a>.</p>';
          break;
        }
      }
      if (error !== '') {
        $('#messages').html('<div class="fail">' + error + '</div>');
        return false;
      } else {
        var url = getAjaxURL('submit');
        $.ajax({
          type: "POST",
          url: url,
          dataType: "json",
          data: input,
          success : function(data){
          }
        });
        var message = 'Thank you for submitting your RSVP! Whether you are able to attend or not we appreciate you taking the time to respond.';
        $('#messages').html('<div class="success"><p>' + message + '</p></div>');
        submit.attr('disabled','disabled');
        return true;
      }
    }
  });
}

function getAjaxURL(type) {
  var input = $('form input:not(.hidden), form textarea').serialize(),
      url;

  if (type == 'validate') {
    url = 'includes/views/form/invitees.php';
  } else if (type == 'submit') {
    url = 'includes/views/form/submit.php';
  }

  if (input.length > 0) {
    if (url.indexOf('?') > -1) {
      url += '&' + input;
    } else {
      url += '?' + input;
    }
  };

  return url;
}

