
$(document).ready(function(){
  $('section.ourstory .slick-carousel').slick({
    prevArrow: '<button type="button" class="slick-prev"></button>',
    nextArrow: '<button type="button" class="slick-next"></button>',
    dots: true,
    infinite: false
  });

  $('div.bridesmaids .slick-carousel').slick({
    prevArrow: '<button type="button" class="slick-prev"></button>',
    nextArrow: '<button type="button" class="slick-next"></button>',
    dots: true,
    infinite: false
  });

  $('div.groomsmen .slick-carousel').slick({
    prevArrow: '<button type="button" class="slick-prev"></button>',
    nextArrow: '<button type="button" class="slick-next"></button>',
    dots: true,
    infinite: false
  });
});

$(document).ready(function() {
  // $('body:not(.mobile) nav ul li').flexVerticalCenter();
  $('section.ourstory .title-header').flexVerticalCenter();
  $('section.googlemap .title').flexVerticalCenter();
  // $('section.bridalparty .profilepic').flexVerticalCenter();
  // $('section.rsvp div.field-text-field label.field-label p').flexVerticalCenter();
  // $('section.rsvp div.field-radios label.radio-option').flexVerticalCenter();
  // $('footer .credit div').flexVerticalCenter();
});

$(document).ready(function() {
  $('input[type=submit]').innerWidth($('input[type=password]').innerWidth());
});