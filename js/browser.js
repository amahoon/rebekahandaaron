var init_val = $(window).scrollTop();

var window_width = $(window).width();
var window_height = $(window).height();

var ipad = ((/iPad/i.test(navigator.userAgent)) === true) ? true : false;
var browser_device = ((/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) === true) ? 'Mobile Device' : 'Browser';

var header = $('header div.mobilemenu');
var head = $('header div.head');
var nav = $('header nav');
var container = $('header nav li.logo');
var logo = container.find('img');
var menucontroller = $('header .mobilemenu-controller');

$(document).ready( function () {
	window_width = $(window).width();

	$('.window-measurements .width').html('<strong>Width:</strong> ' + window_width);
	$('.window-measurements .height').html('<strong>Height:</strong> ' + window_height);
	$('.window-measurements .device').html('<strong>Device:</strong> ' + ipad);

	if (browser_device == 'Browser') {
		if (window_width > 607) {
			desktopNav();
		} else {
			mobileNav();
		}

		if (window_width > 464) {
			$('body').removeClass('mobile');
			head.height('auto');
		} else {
			$('body').addClass('mobile');
			head.height(header.height());
		}

		// if (window_width < 992 && window_width >= 768) {
		if (window_width < 992) {
			mobileEvents(window_width);
		} else {
			desktopEvents();
		}
	} else {
		mobileNav();
	}

  menucontroller.click(function() {
    nav.toggleClass('open');
    $('body').toggleClass('fixed');
  });

});

function desktopNav() {
	if ($('header div.logo').length > 0) {
	  var logoHolder = $('header div.logo');
	  logoHolder.remove();
	  container.removeClass('hidden');
	}

	head.height('auto');
}

function desktopEvents() {
	if ($('section.event > .information').length > 0) {
	 	var event = $('section.event');
	  var info = $('section.event .details div.information');
	  var divider = $('section.event .details .divider');
	  var mobileInfo = $('section.event > .information');

	  mobileInfo.remove();

	  info.removeClass('hidden');
	  divider.removeClass('hidden');
	}

	if ($('section.rsvp form .field-name--email .hint:not(.email)').length > 0) {
		var hint = $('section.rsvp form .field-name--email .hint:not(.email)');
		hint.remove();
	}
}

function mobileNav() {
	if ($('header div.logo').length == 0) {
		var logoHolder = document.createElement('div');
	  logoHolder.setAttribute('class','logo');
	  header.prepend(logoHolder);

	  var logoHolder = $('header div.logo');
	  logo.clone().appendTo(logoHolder);
	  container.addClass('hidden');
	}
}

function mobileEvents(window_width) {
	if (window_width < 992 && window_width >= 768) {
		eventInfo();
	}
	if (window_width < 992) {
		rsvpHint();
	}

	function eventInfo() {
		if ($('section.event > .information').length == 0) {
		 	var event = $('section.event');
		  var info = $('section.event .details div.information');
		  var divider = $('section.event .details .divider');
		  var clone = info.clone();

		  clone.addClass('mobile');
		  clone.appendTo(event);
		  info.addClass('hidden');
		  divider.addClass('hidden');

		  // $('.sidebar.side-right').flexVerticalCenter();
		}
	}

	function rsvpHint() {
		if ($('section.rsvp form .field-name--email .hint:not(.email)').length == 0) {
			var rsvp = $('section.rsvp');
		  var emailQuestions = $('section.rsvp form .field-name--email .hint.email');
		  var emailLabel = $('section.rsvp form .field-name--email label.field-label');
		  var whyweneedit = $('section.rsvp form .field-name--email p.whyweneedit');
		  var createHint = document.createElement('span');
		  createHint.setAttribute('class','hint');

		  var label = document.querySelector("section.rsvp form .field-name--email label.field-label");
		  label.insertBefore(createHint, label.childNodes[5]);
		  var hint = $('section.rsvp form .field-name--email .hint:not(.email)');
		  hint.html(whyweneedit.html());
		}
	}
}

if (browser_device == 'Browser') {
	$( window ).resize(function() {
		window_width = $(window).width();
		window_height = $(window).height();
		
		if (window_width > 607) {
			desktopNav();
		} else {
			mobileNav();
		}
		if (window_width > 464) {
			$('body').removeClass('mobile');
			head.height('auto');
		} else {
			$('body').addClass('mobile');
			head.height(header.height());
		}

		// if (window_width < 992 && window_width >= 768) {
		if (window_width < 992) {
			mobileEvents(window_width);
		} else {
			desktopEvents();
		}

		$('body:not(.mobile) nav ul li').flexVerticalCenter();

		$('.window-measurements .width').html('<strong>Width:</strong> ' + window_width);
		$('.window-measurements .height').html('<strong>Height:</strong> ' + window_height);
	});
}