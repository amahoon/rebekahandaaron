function initMap() {
  var loc = { lat: 43.623425, lng: -79.386547 };
  var api_key = '94338e4c-2f82-44f7-b4ca-df3cbf99eb70';
  var url = 'https://snazzymaps.com/favorites.json?key=' + api_key;

  var style = [];

  // Set the global configs to synchronous 
  $.ajaxSetup({
    async: false
  });

  $.getJSON(url, function(data) {
    var json = '';
    var totalPages = data.pagination.totalPages;
    for (var i = 1; i < totalPages + 1; i++) {
      // cycle through each page of the favourites
      var pageUrl = url + '&page=' + i;
      // json isn't an empty string (meaning we have found the style for the desired ID), break the page url loop
      if (json) {
        break;
      }
      $.getJSON(pageUrl, function(data) {
        for (var x = 0; x < data.styles.length; x++) {
          var id = data.styles[x].id;
          if (id == 141215) {
            json = data.styles[x].json;
            style = $.parseJSON(json);

            break;
          }
        }
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 13,
          center: loc,
          styles: style,
        });

        var marker = new google.maps.Marker({
          position: loc,
          icon: 'images/mapMarker.png',
          animation: google.maps.Animation.DROP,
          map: map
        });
      });
    }
  });

  // Set the global configs back to asynchronous 
  $.ajaxSetup({
      async: true
  });
}