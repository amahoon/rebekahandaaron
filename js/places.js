$(document).ready( function () {
  // var api_key = 'AIzaSyBy23tMgKgGrhnOSFNi0AfWoFLaHzD9lfc',
  //     location = '43.623425,-79.386547',
  //     radius = '5000',
  //     type = 'lodging',
  //     place_id = 'ChIJX-YHICo1K4gR1SQ3P5SL7EI&key=AIzaSyBy23tMgKgGrhnOSFNi0AfWoFLaHzD9lfc';

  // var search_url = 'https://maps.googleapis.com/maps/api/place/nearbysearch/json';
  // var search_params = 'location=' + location + '&radius=' + radius + '&type=' + type + '&key=' + api_key;
  // var place_url = 'https://maps.googleapis.com/maps/api/place/details/json';
  // var place_params = 'placeid=' + place_id + '&key=' + api_key;
  // // console.log('https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=' + location + '&radius=' + radius + '&type=' + type + '&key=' + api_key);
  // console.log(search_url + '?' + search_params);
  // // console.log('https://maps.googleapis.com/maps/api/place/details/json?placeid=' + place_id + '&key=' + api_key);
  // console.log(place_url + '?' + place_params);
  
  var hotels = $('section.event .hotels .results');
  var url = "includes/places.php";
  // var numOfRes = 5;
  var numOfRes = 4;
  // console.log(url);
  var input = $('form input').serialize();
  var stars = '<span class="star star--1"></span><span class="star star--2"></span><span class="star star--3"></span><span class="star star--4"></span><span class="star star--5"></span>';

  $.ajax({
    type: "POST",
    url: url,
    dataType: "json",
    data: input,
    success : function(data){
      var hotel = '', count = 0, html;
      for(i=0;i<numOfRes;i++) {
        html = hotels.html();
        var result = data.results[i];
        // console.log(result);
        var name = result.name,
            placeID = result.place_id,
            rating = result.rating,
            openNow = result.opening_hours.open_now,
            address = result.vicinity;
        count = i + 1;
        // console.log(name);
        hotel = '<li class="hotel hotel--' + count + '" id="hotel-' + placeID + '">';

        hotel += '<div class="hotel--name"><p>' + name + '</p></div>';
        hotel += '<div class="hotel--rating">';
          hotel += '<p class="hotel--rating-number">' + rating + '</p>';
          hotel += '<p class="hotel--star-rating rating--' + Math.floor(rating) + '">' + stars + '</p>';
        hotel += '</div>';
        // hotel += '<p class="hotel--opennow">Open Now: ' + openNow + '</p>';
        // hotel += '<p class="hotel--address"><span class="icon"></span>' + address + '</p>';

        hotel += '<div class="details"></div>';
        hotel += '</li>';
        hotels.html(html + hotel);

        var url = "includes/placeid.php?placeID=" + placeID;
        // console.log(url);
        $.ajax({
          type: "POST",
          url: url,
          dataType: "json",
          data: input,
          success : function(place){
            var details = '', result = place.result;
            // console.log(result);
            var placeID = result.place_id;
            var addressComps = result.address_components;
            var phone = result.formatted_phone_number,
                intPhone = result.international_phone_number,
                hours = result.opening_hours.weekday_text,
                techHours = result.opening_hours.periods,
                photos = result.photos,
                reviews = result.reviews,
                website = result.website,
                mapURL = result.url,
                thisHotel = $('.hotels #hotel-' + placeID + ' .details'),
                address = addressComps[0].long_name + ' ' + addressComps[1].short_name + ', ' + addressComps[3].short_name + ', ' + addressComps[5].short_name + ' ' + addressComps[7].short_name,
                shortAddress = addressComps[0].long_name + ' ' + addressComps[1].short_name + ', ' + addressComps[3].short_name;
            var html = thisHotel.html();

            details += '<p class="hotel--address"><span class="icon"></span><a href="' + mapURL + '" target="_blank">' + shortAddress + '</a></p>';
            details += '<p class="hotel--phone"><span class="icon"></span>' + '<a href="tel:' + intPhone.replace(' ','').replace(/-/g,'') + '">' + phone + '</a></p>';
            details += '<p class="hotel--website"><span class="icon"></span><a href="' + mapURL + '" target="_blank">' + stripURL(website) + '</a></p>';
            
            // details += '<div class="hotel--hours-of-operation">';
            // for(i=0;i<hours.length;i++) {
            //   details += '<p>' + hours[i] + '</p>';
            // }
            // details += '</div>';
            
            // details += '<div class="viewReviews closed"><p>view reviews</p></div>';
            // details += '<div class="hotel--reviews">';
            // for(i=0;i<3;i++) {
            //   var maxLength = 150, review;
            //   if (reviews[i].text.length >= maxLength) {
            //     review = reviews[i].text.substring(0,maxLength).trim() + '...';
            //   } else {
            //     review = reviews[i].text.trim();
            //   }
            //   details += '<div class="review-container">';
            //     details += '<div class="reviewer">';
            //       details += '<p class="reviewer--name"><span class="label">' + reviews[i].author_name + '</span></p>';
            //     details += '</div>';
            //     details += '<div class="review">';
            //       details += '<p class="review--star-rating rating--' + reviews[i].rating + '">' + stars + '</p>';
            //       details += '<p class="review--time-ago">' + reviews[i].relative_time_description + '</p>';
            //       details += '<p class="review--review--short">' + review + '</p>';
            //       details += '<p class="review--review--long">' + reviews[i].text + '</p>';
            //     details += '</div>';
            //   details += '</div>';
            // }
            // details += '</div>';

            if (typeof details !== 'undefined') {
              thisHotel.html(html + details);
            }
          },
          complete : function(data) {
            $('.hotels li.hotel').matchHeight();
            // $('.hotels li.hotel div.hotel--name').matchHeight();
            // $('.viewReviews.closed').click(function() {
            //   console.log($(this));
            //   var reviews = $(this).next('.hotel--reviews');
            //   reviews.addClass('open');
            //   $(this).find('p').html('hide reviews');
            //   $(this).addClass('open');
            //   $(this).removeClass('closed');
            // });
            // $('.viewReviews.open').click(function() {
            //   console.log($(this));
            //   var reviews = $(this).next('.hotel--reviews');
            //   reviews.removeClass('open');
            //   $(this).find('p').html('view reviews');
            //   $(this).removeClass('open');
            //   $(this).addClass('closed');
            // });
          }
        });
      }
    }
  });

  // console.log($('.viewReviews'));
  // $('.viewReviews').click(function() {
  // //   // var details = $(this).parent().parent().next('.year-details');
  // //   console.log($(this).parent());
  // });

});

function stripURL(url) {
  url = url.replace('https://www.','');
  url = url.replace('http://www.','');
  url = url.substring(0,url.indexOf('/'));

  return url;
}

function createLabel(str) {
  return '<span class="label">' + str + ': </span>';
}

