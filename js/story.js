var readmoreLinks = $('section.ourstory .slide .readmore p');

$(document).ready(function(){

  readmoreLinks.each(function() {
    $(this).click(function(event) {
      var details = $(this).parent().parent().next('.year-details');
      details.removeClass('closed');
      var close = details.find('.close');
      close.click(function(event) {
        details.addClass('closed');
      });
    });
  });

});