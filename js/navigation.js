$(document).ready(function() {
  var nav = $('nav ul li:not(.logo) p');
  nav.each(function(index, el) {
    $(this).click(function(event) {
      var classes = $(this).attr('class');
      if (classes !== undefined) {
        $('html, body').animate({
          scrollTop: ($('#' + classes).offset().top)
        },500);
      }
    });
  });

});